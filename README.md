# Reversi MVC
By [Jasper van Veenhuizen](https://jasperv.dev/).

**School:** Windesheim University of Applied Sciences  
**Minor:** Web Development  
**Class:** Server Technology

This project is an ASP.NET Core MVC Application 
which handles the player authentication&authorisation.  
This app acts like a client for the player and serves the front-end.

- MVC Client Application is found in the [current repository](https://gitlab.com/Putper/reversi-mvc).
- API handling game logic can be found [here](https://gitlab.com/Putper/reversi-api).
- Resources that provide the MVC app with JavaScript, CSS and images can be found [here](https://gitlab.com/Putper/reversi-resources).


## Setup
To setup this project you must first setup the [Reversi API](https://gitlab.com/Putper/reversi-api).
Then configure this project so it can communicate with it.

### Configure
In the ReversiMvcApp folder, make a copy of `envsettings.json.example` and name it `envsettings.json`.
```bash
$ cp ReversiMvcApp/envsettings.json.example ReversiMvcApp/envsettings.json
```

Open your newly created `envsettings.json` and configure the API settings by setting the `Uri` and `Secret`.  
Secret key must match the secret set in the API configuration.
```bash
$ vim ReversiMvcApp/envsettings.json
```

