﻿using System.Collections;
using System.Collections.Generic;

namespace ReversiRestApi.Data
{
    public class ApplicationClaimTypes : IEnumerable
    {
        public const string Player = "Player";
        public const string Mediator = "Mediator";
        public const string Admin = "Admin";

        private readonly IEnumerable<string> _claims = new List<string> {Player, Mediator, Admin};

        public IEnumerator GetEnumerator()
        {
            return _claims.GetEnumerator();
        }
    }
}
