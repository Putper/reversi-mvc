﻿using ReversiMvcApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReversiMvcApp.Data
{
    public interface IReversiApi
    {
        Task<string> GetApiTokenForPlayer(string id, IList<string> roles);
        Task<IEnumerable<Game>> GetGames();
        Task<IEnumerable<Game>> GetGamesLookingForPlayers();
        Task<IEnumerable<Game>> GetGamesWithPlayer(string playerToken);
        Task<Game> GetGame(string token);
        Task<Game> CreateGame(Game game);
        Task<Game> JoinGame(string gameToken, string playerToken);
        Task<Game> SurrenderGame(string gameToken, string playerToken);
        Task DeleteGame(string gameToken, string playerToken);
    }
}
