﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ReversiMvcApp.Extensions;
using ReversiMvcApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ReversiMvcApp.Data
{
    public class ReversiApi : IReversiApi
    {
        private HttpClient _client { get; set; }

        public ReversiApi(IOptions<Apis> apis)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(apis.Value.Reversi.Uri);
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apis.Value.Reversi.Secret);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> GetApiTokenForPlayer(string id, IList<string> roles)
        {
            HttpResponseMessage response = await _client.PostAsJsonAsync($"api/users/token", new { id, roles });
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadFromJsonAsync<string>();
        }

        /// <summary>
        /// Get all games
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Game>> GetGames()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/game");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Game>>(
                await response.Content.ReadAsStringAsync()
            );
            // using Microsoft.AspNet.WebApi.Client:
            // return await response.Content.ReadAsAsync<List<Game>>();
        }

        /// <summary>
        /// Get games looking for players
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Game>> GetGamesLookingForPlayers()
        {
            HttpResponseMessage response = await _client.GetAsync("/api/game/looking");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<List<Game>>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// Get games with a specific player
        /// </summary>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Game>> GetGamesWithPlayer(string playerToken)
        {
            if (string.IsNullOrEmpty(playerToken))
            {
                return await GetGamesLookingForPlayers();
            }

            HttpResponseMessage response = await _client.GetAsync("/api/game?playerToken=" + playerToken);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<List<Game>>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// Get game using game token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Game> GetGame(string token)
        {
            HttpResponseMessage response = await _client.GetAsync("/api/game/" + token);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<Game>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// Register a game in API
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public async Task<Game> CreateGame(Game game)
        {
            HttpResponseMessage response = await _client.PostAsJsonAsync("/api/game", game);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<Game>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// join a game as given player
        /// </summary>
        /// <param name="gameToken">token of game to join</param>
        /// <param name="playerToken">token of player who wants to join</param>
        /// <returns>game that has been joined</returns>
        public async Task<Game> JoinGame(string gameToken, string playerToken)
        {
            HttpResponseMessage response = await _client.PutAsJsonAsync($"api/game/{gameToken}/join", playerToken);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<Game>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// Surrender a game as a player
        /// </summary>
        /// <param name="gameToken">token of game to surrender</param>
        /// <param name="playerToken">token of player who surrenders</param>
        /// <returns>game that has been surrendered</returns>
        public async Task<Game> SurrenderGame(string gameToken, string playerToken)
        {
            HttpResponseMessage response = await _client.PutAsJsonAsync($"api/game/{gameToken}/surrender", playerToken);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<Game>(
                await response.Content.ReadAsStringAsync()
            );
        }

        /// <summary>
        /// Surrender a game as a player
        /// </summary>
        /// <param name="gameToken">token of game to surrender</param>
        /// <param name="playerToken">token of player who surrenders</param>
        /// <returns>game that has been surrendered</returns>
        public async Task DeleteGame(string gameToken, string playerToken)
        {
            HttpResponseMessage response = await _client.DeleteAsJsonAsync($"api/game/{gameToken}", playerToken);
            response.EnsureSuccessStatusCode();
        }
    }
}
