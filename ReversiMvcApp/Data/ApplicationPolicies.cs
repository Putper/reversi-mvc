﻿namespace ReversiMvcApp.Data
{
    public class ApplicationPolicies
    {
        public const string AdminOnly = "AdminOnly";
        public const string MediatorOnly = "MediatorOnly";
        public const string PlayerOnly = "PlayerOnly";
    }
}
