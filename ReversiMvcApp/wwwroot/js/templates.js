this["spa_templates"] = this["spa_templates"] || {};
this["spa_templates"]["src"] = this["spa_templates"]["src"] || {};
this["spa_templates"]["src"]["templates"] = this["spa_templates"]["src"]["templates"] || {};
this["spa_templates"]["src"]["templates"]["cat"] = this["spa_templates"]["src"]["templates"]["cat"] || {};
this["spa_templates"]["src"]["templates"]["cat"]["cat"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"cat--container\" style='background-image: url(\""
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"catImageUrl") || (depth0 != null ? lookupProperty(depth0,"catImageUrl") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"catImageUrl","hash":{},"data":data,"loc":{"start":{"line":1,"column":58},"end":{"line":1,"column":73}}}) : helper)))
    + "\")'>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"cat.fact"),depth0,{"name":"cat.fact","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "</div>";
},"usePartial":true,"useData":true});
Handlebars.registerPartial("cat.fact", Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"catfact--container\">\r\n    <span class=\"catfact\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"catFact") || (depth0 != null ? lookupProperty(depth0,"catFact") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"catFact","hash":{},"data":data,"loc":{"start":{"line":2,"column":26},"end":{"line":2,"column":37}}}) : helper)))
    + "</span>\r\n</div>";
},"useData":true}));
this["spa_templates"]["src"]["templates"]["feedbackWidget"] = this["spa_templates"]["src"]["templates"]["feedbackWidget"] || {};
this["spa_templates"]["src"]["templates"]["feedbackWidget"]["body"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"body\">\r\n    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"message") || (depth0 != null ? lookupProperty(depth0,"message") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"message","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":15}}}) : helper)))
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","hash":{"black":false},"data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","hash":{"black":true},"data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "</section>\r\n";
},"usePartial":true,"useData":true});
Handlebars.registerPartial("game.disc", Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"board__disc board__disc--black\"></div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"board__disc\"></div>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"black") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":5,"column":7}}})) != null ? stack1 : "");
},"useData":true}));
this["spa_templates"]["src"]["templates"]["game"] = this["spa_templates"]["src"]["templates"]["game"] || {};
this["spa_templates"]["src"]["templates"]["game"]["board"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"game.tile"),depth0,{"name":"game.tile","data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"board\"\r\n     style=\"grid-template-columns: repeat("
    + alias4(((helper = (helper = lookupProperty(helpers,"boardHeight") || (depth0 != null ? lookupProperty(depth0,"boardHeight") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"boardHeight","hash":{},"data":data,"loc":{"start":{"line":2,"column":42},"end":{"line":2,"column":57}}}) : helper)))
    + ", 1fr); grid-template-rows: repeat("
    + alias4(((helper = (helper = lookupProperty(helpers,"boardHeight") || (depth0 != null ? lookupProperty(depth0,"boardHeight") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"boardHeight","hash":{},"data":data,"loc":{"start":{"line":2,"column":92},"end":{"line":2,"column":107}}}) : helper)))
    + ", 1fr)\"\r\n>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"tiles") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":4},"end":{"line":6,"column":13}}})) != null ? stack1 : "")
    + "</div>\r\n";
},"usePartial":true,"useData":true});
Handlebars.registerPartial("game.tile", Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","hash":{"black":(depth0 != null ? lookupProperty(depth0,"discIsBlack") : depth0)},"data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"board__tile "
    + alias4(((helper = (helper = lookupProperty(helpers,"backgroundClass") || (depth0 != null ? lookupProperty(depth0,"backgroundClass") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backgroundClass","hash":{},"data":data,"loc":{"start":{"line":1,"column":24},"end":{"line":1,"column":43}}}) : helper)))
    + "\" data-row=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"row") || (depth0 != null ? lookupProperty(depth0,"row") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"row","hash":{},"data":data,"loc":{"start":{"line":1,"column":55},"end":{"line":1,"column":62}}}) : helper)))
    + "\" data-column=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"column") || (depth0 != null ? lookupProperty(depth0,"column") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"column","hash":{},"data":data,"loc":{"start":{"line":1,"column":77},"end":{"line":1,"column":87}}}) : helper)))
    + "\">\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasDisc") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":4,"column":11}}})) != null ? stack1 : "")
    + "</div>";
},"usePartial":true,"useData":true}));
this["spa_templates"]["src"]["templates"]["game"]["disc"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
this["spa_templates"]["src"]["templates"]["game"]["playerlist"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "playerlist__disc--active";
},"3":function(container,depth0,helpers,partials,data) {
    return "<b>";
},"5":function(container,depth0,helpers,partials,data) {
    return " (you)";
},"7":function(container,depth0,helpers,partials,data) {
    return "</b>";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"playerlist\">\r\n    <div class=\"playerlist__player\">\r\n        <div class=\"playerlist__disc "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":37},"end":{"line":3,"column":86}}})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","hash":{"black":false},"data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </div>\r\n        <div>\r\n            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":12},"end":{"line":7,"column":40}}})) != null ? stack1 : "")
    + "\r\n            "
    + alias4(((helper = (helper = lookupProperty(helpers,"whiteName") || (depth0 != null ? lookupProperty(depth0,"whiteName") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"whiteName","hash":{},"data":data,"loc":{"start":{"line":8,"column":12},"end":{"line":8,"column":25}}}) : helper)))
    + "\r\n            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"playerIsWhite") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":12},"end":{"line":9,"column":46}}})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":12},"end":{"line":10,"column":41}}})) != null ? stack1 : "")
    + "\r\n        </div>\r\n    </div>\r\n    <div class=\"playerlist__player\">\r\n        <div class=\"playerlist__disc "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":14,"column":37},"end":{"line":14,"column":94}}})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"game.disc"),depth0,{"name":"game.disc","hash":{"black":true},"data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </div>\r\n        <div>\r\n            "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":12},"end":{"line":18,"column":48}}})) != null ? stack1 : "")
    + "\r\n            "
    + alias4(((helper = (helper = lookupProperty(helpers,"blackName") || (depth0 != null ? lookupProperty(depth0,"blackName") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"blackName","hash":{},"data":data,"loc":{"start":{"line":19,"column":12},"end":{"line":19,"column":25}}}) : helper)))
    + "\r\n            "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"playerIsWhite") : depth0),{"name":"unless","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":12},"end":{"line":20,"column":54}}})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"whitesTurn") : depth0),{"name":"unless","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":21,"column":12},"end":{"line":21,"column":49}}})) != null ? stack1 : "")
    + "\r\n        </div>\r\n    </div>\r\n</div>\r\n";
},"usePartial":true,"useData":true});