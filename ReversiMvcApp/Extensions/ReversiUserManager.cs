﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ReversiMvcApp.Data;
using ReversiMvcApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ReversiMvcApp.Extensions
{
    public class ReversiUserManager : UserManager<IdentityUser>
    {
        private readonly ReversiDbContext _context;
        private readonly IReversiApi _api;

        public ReversiUserManager(IUserStore<IdentityUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<IdentityUser> passwordHasher, IEnumerable<IUserValidator<IdentityUser>> userValidators, IEnumerable<IPasswordValidator<IdentityUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<IdentityUser>> logger, ReversiDbContext context, IReversiApi api)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _context = context;
            _api = api;
        }

        public async Task FullDeleteAsync(string userId)
        {
            IdentityUser user = await Users.Where(user => user.Id == userId).FirstOrDefaultAsync();
            await FullDeleteAsync(user);
        }

        public async Task FullDeleteAsync(IdentityUser user)
        {
            // delete user
            await DeleteAsync(user);
            // delete player
            Player player = await _context.Players.FindAsync(user.Id);
            if (player != null) _context.Players.Remove(player);

            //// log
            //string currentUserName = _httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.Name).Value;

            // surrender any running games
            var games = await _api.GetGamesWithPlayer(user.Id);
            var unfinishedGames = games.Where(game => game.IsFinished == false).ToList();
            foreach (var game in unfinishedGames)
            {
                if (game.Player1Token == null || game.Player2Token == null)
                    await _api.DeleteGame(game.Token, user.Id);
                else
                    await _api.SurrenderGame(game.Token, user.Id);

                // in the MVC's database running games are also tracked,
                // delete from there as well
                var runningGame = await _context.RunningGames.FindAsync(game.Token);
                if (runningGame != null)
                {
                    _context.RunningGames.Remove(runningGame);
                }
            }

            await _context.SaveChangesAsync();
        }
    }
}
