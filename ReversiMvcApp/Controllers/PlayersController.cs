﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ReversiMvcApp.Data;
using ReversiMvcApp.Models;

namespace ReversiMvcApp.Controllers
{
    [Authorize(Policy = ApplicationPolicies.PlayerOnly)]
    public class PlayersController : Controller
    {
        private readonly ReversiDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public PlayersController(ReversiDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Players
        public async Task<IActionResult> Index()
        {
            List<Player> players = (List<Player>)await GetPlayersWithNames();
            SortPlayersByScore(players);
            players.Reverse(); // best at the top
            return View(players);
        }

        /// <summary>
        /// Get all players and fill the name property
        /// </summary>
        /// <returns>players with name property set</returns>
        private async Task<IList<Player>> GetPlayersWithNames()
        {
            var players = await _context.Players.ToListAsync();
            foreach(Player player in players)
            {
                var user = _userManager.Users.Where(user => user.Id == player.Token).FirstOrDefault();
                player.Name = user?.UserName;
            }
            return players;
        }
        
        /// <summary>
        /// sort playerlist based on score. score is based on their win/lose stats
        /// </summary>
        /// <param name="players">list of players to sort</param>
        private void SortPlayersByScore(List<Player> players)
        {
            players.Sort((Player p1, Player p2) =>
            {
                return GetScore(p1).CompareTo(GetScore(p2));
            });
        }

        /// <summary>
        /// Score player based on amount won, tied and lost
        /// </summary>
        /// <param name="player">player to score</param>
        /// <returns>score for player</returns>
        private int GetScore(Player player)
        {
            return player.TimesWon*2 + player.TimesTied - player.TimesLost*2;
        }
    }
}
