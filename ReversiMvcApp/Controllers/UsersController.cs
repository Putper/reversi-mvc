﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ReversiMvcApp.Data;
using ReversiMvcApp.Extensions;
using ReversiMvcApp.Models;
using ReversiMvcApp.Models.ViewModels;
using ReversiRestApi.Data;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ReversiMvcApp.Controllers
{
    [Authorize(Policy = ApplicationPolicies.MediatorOnly)]
    public class UsersController : Controller
    {
        private readonly IReversiApi _api;
        private readonly ReversiUserManager _userManager;
        private readonly ILogger<UsersController> _logger;
        private readonly ReversiDbContext _context;

        public UsersController(IReversiApi api, ReversiUserManager userManager, ILogger<UsersController> logger, ReversiDbContext context)
        {
            _api = api;
            _userManager = userManager;
            _logger = logger;
            _context = context;
        }


        // GET: Users
        public ActionResult Index()
        {
            var users = _userManager.Users.ToList();
            bool isAdmin = User.HasClaim(ClaimTypes.Role, ApplicationClaimTypes.Admin);
            var viewModel = new UsersViewModel(users, isAdmin);
            return View(viewModel);
        }


        // GET: Users/Edit/user-id
        [Authorize(Policy = ApplicationPolicies.AdminOnly)]
        public async Task<ActionResult> Edit(string id)
        {
            IdentityUser targetUser = await _userManager.Users.Where(user => user.Id == id).FirstOrDefaultAsync();
            IList<Claim> targetClaims = await _userManager.GetClaimsAsync(targetUser);

            // cannot edit other admins
            if(targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Admin).FirstOrDefault() != null)
                return RedirectToAction(nameof(Index));

            bool isMediator = targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Mediator).FirstOrDefault() != null;
            var viewModel = new EditUserViewModel(targetUser, isMediator);
            return View(viewModel);
        }

        // POST: Users/Edit/user-id
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = ApplicationPolicies.AdminOnly)]
        public async Task<ActionResult> Edit(string id, bool isMediator)
        {
            try
            {
                IdentityUser targetUser = await _userManager.Users.Where(user => user.Id == id).FirstOrDefaultAsync();

                // cannot edit other admins
                IList<Claim> targetClaims = await _userManager.GetClaimsAsync(targetUser);
                if (targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Admin).FirstOrDefault() != null)
                    return RedirectToAction(nameof(Index));

                var claims = User.Claims;
                string currentUserId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                string currentUserName = User.FindFirst(ClaimTypes.Name)?.Value;
                Claim existingMediatorClaim = targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Mediator).FirstOrDefault();
                // give mediatorclaim if he needs it, but doesnt have it yet
                if (existingMediatorClaim == null && isMediator)
                {
                    await _userManager.AddClaimAsync(targetUser, new Claim(ClaimTypes.Role, ApplicationClaimTypes.Mediator));
                    _logger.LogInformation($"User made Mediator: \"{targetUser.Id}\" (\"{targetUser.UserName}\")" +
                        $" Action performed by \"{currentUserId}\" ({currentUserName})");
                }
                // take away mediator claim if he has it, but shouldn't have it anymore
                else if (existingMediatorClaim != null && !isMediator)
                {
                    await _userManager.RemoveClaimAsync(targetUser, existingMediatorClaim);
                    _logger.LogInformation($"User removed from Mediator: \"{targetUser.Id}\" (\"{targetUser.UserName}\")" +
                        $" Action performed by \"{currentUserId}\" ({currentUserName})");
                }

                // log targetUser out to refresh their permissions
                await _userManager.UpdateSecurityStampAsync(targetUser);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(Edit), id);
            }
        }

        // GET: Users/Delete/user-id
        public async Task<ActionResult> Delete(string id)
        {
            var targetUser = await _userManager.Users.Where(user => user.Id == id).FirstOrDefaultAsync();
            if (targetUser == null || !await MayDeleteUser(targetUser))
                return RedirectToAction(nameof(Index));

            return View(targetUser);
        }

        // POST: Users/Delete/user-id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string id, IFormCollection collection)
        {
            try
            {
                // ensure is allowed to delete
                var targetUser = await _userManager.Users.Where(user => user.Id == id).FirstOrDefaultAsync();
                if (!await MayDeleteUser(targetUser))
                    return RedirectToAction(nameof(Delete), id);

                await _userManager.FullDeleteAsync(targetUser);

                // log
                string currentUserName = User.FindFirst(ClaimTypes.Name).Value;
                _logger.LogInformation($"User deleted: \"{targetUser.Id}\" (\"{targetUser.UserName}\")." +
                    $" Action performed by {currentUserName}");

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// If the current user has the permission to delete the targeted user
        /// </summary>
        /// <param name="targetUser">user to delete</param>
        /// <returns></returns>
        private async Task<bool> MayDeleteUser(IdentityUser targetUser)
        {
            bool amAdmin = User.HasClaim(ClaimTypes.Role, ApplicationClaimTypes.Admin);
            var targetClaims = await _userManager.GetClaimsAsync(targetUser);
            bool targetIsAdmin = targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Admin).FirstOrDefault() != null;
            bool targetIsMediator = targetClaims.Where(claim => claim.Value == ApplicationClaimTypes.Mediator).FirstOrDefault() != null;

            return (
                amAdmin && !targetIsAdmin // admins can delete anyone except other admins
                || !targetIsAdmin && !targetIsMediator // mediators can only delete regular players
            );
        }
    }
}
