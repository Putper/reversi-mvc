﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReversiMvcApp.Data;
using ReversiMvcApp.Models;
using ReversiMvcApp.Models.ViewModels;

namespace ReversiMvcApp.Controllers
{
    [Authorize(Policy = ApplicationPolicies.PlayerOnly)]
    public class GamesController : Controller
    {
        private readonly ReversiDbContext _context;
        private readonly IReversiApi _api;
        private readonly string _apiUri;
        private readonly UserManager<IdentityUser> _userManager;

        public GamesController(ReversiDbContext context, UserManager<IdentityUser> userManager, IReversiApi api, IOptions<Apis> apis)
        {
            _context = context;
            _api = api;
            _apiUri = apis.Value.Reversi.Uri;
            _userManager = userManager;
        }


        // GET: Games
        public async Task<IActionResult> Index()
        {
            // redirect to game if there's still an unfinished game
            Game unfinishedGame = await GetUnfinishedGame();
            if (unfinishedGame != null)
                return RedirectToAction(nameof(Play), new { token = unfinishedGame.Token });

            IList<Game> games = (await _api.GetGamesLookingForPlayers()).ToList<Game>();
            return View(games);
        }


        /// <summary>
        /// GET: Games/{token}
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("Games/{token}")]
        public async Task<IActionResult> Play(string token)
        {
            // game must exist
            if (string.IsNullOrEmpty(token))
                return NotFound();
            Game game = await _api.GetGame(token);
            if (game == null)
                return NotFound();

            // player must be player in game
            string playerToken = GetPlayerToken();
            if (game.Player1Token != playerToken && game.Player2Token != playerToken)
                return RedirectToAction(nameof(Index));

            string apiToken = await GetApiTokenForPlayer(playerToken);

            // if game not started yet, go to waiting view
            if (game.Player1Token == null || game.Player2Token == null)
                return View("WaitingForPlayer", new WaitingForPlayerViewModel(game, _apiUri, apiToken));

            // get player info
            string currentPlayerName = User.Identity.Name;
            string colour;
            string whiteName;
            string blackName;
            if(playerToken == game.Player1Token)
            {
                colour = "White";
                whiteName = currentPlayerName;
                blackName = GetPlayerName(game.Player2Token);
            }
            else
            {
                colour = "Black";
                blackName = currentPlayerName;
                whiteName = GetPlayerName(game.Player1Token);
            }

            return View("Game", new GameViewModel(game, colour, GetPlayerToken(), whiteName, blackName, _apiUri, apiToken));
        }


        /// <summary>
        /// GET: Games/{token}/Join
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        [HttpGet("Games/{token}/Join")]
        public async Task<IActionResult> Join(string token)
        {
            string playerToken = GetPlayerToken();
            Game joinedGame = await _api.JoinGame(token, playerToken);
            return RedirectToAction(nameof(Play), new { token = joinedGame.Token });
        }


        // GET: Games/Create
        [HttpGet("Games/Create")]
        public async Task<IActionResult> Create()
        {
            // redirect to game if there's still an unfinished game
            Game unfinishedGame = await GetUnfinishedGame();
            if (unfinishedGame != null)
                return RedirectToAction(nameof(Play), new { token = unfinishedGame.Token });

            return View();
        }


        // POST: Games/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Description")] Game game)
        {
            if (ModelState.IsValid)
            {
                game.Player1Token = GetPlayerToken();
                // create in api
                Game newGame = await _api.CreateGame(game);
                // monitor locally too
                _context.RunningGames.Add(new RunningGame(newGame.Token));
                await _context.SaveChangesAsync();
                
                return RedirectToAction(nameof(Index));
            }
            return View(game);
        }


        /// <summary>
        /// Finish current game
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("Games/{token}/Finish")]
        public async Task<IActionResult> Finish(string token)
        {
            if (token == null)
                return NotFound();
            RunningGame runningGame = await _context.RunningGames.FindAsync(token);

            // get finished game data
            Game game = await _api.GetGame(token);
            if (game == null || !game.IsFinished)
                return NotFound();

            // get players
            Player whitePlayer = await _context.Players.FindAsync(game.Player1Token);
            if (whitePlayer == null)
            {
                whitePlayer = new Player(game.Player1Token);
                _context.Players.Add(whitePlayer);
            }
            Player blackPlayer = await _context.Players.FindAsync(game.Player2Token);
            if (blackPlayer == null)
            {
                blackPlayer = new Player(game.Player2Token);
                _context.Players.Add(blackPlayer);
            }

            // mark winner and check if current player won
            var viewModel = new FinishedGameViewModel(GameResult.Lost);
            string playerToken = GetPlayerToken();
            switch (game.Winner)
            {
                case Colour.White:
                    whitePlayer.TimesWon++;
                    blackPlayer.TimesLost++;
                    if (game.Player1Token == playerToken)
                        viewModel.Result = GameResult.Won;
                    break;
                case Colour.Black:
                    whitePlayer.TimesLost++;
                    blackPlayer.TimesWon++;
                    if (game.Player2Token == playerToken)
                        viewModel.Result = GameResult.Won;
                    break;
                case Colour.None:
                    whitePlayer.TimesTied++;
                    blackPlayer.TimesTied++;
                    viewModel.Result = GameResult.Tied;
                    break;
            }

            // if the game was still running, mark it as not running
            // and save new score stats
            if (runningGame != null)
            {
                // remove from running games so it can't be marked as won again
                _context.RunningGames.Remove(runningGame);
                await _context.SaveChangesAsync();
            }

            return View(viewModel);
        }



        /// <summary>
        /// Get a JWT token from the API.
        /// The client can use this JWT token to communicate their game actions directly with the API.
        /// </summary>
        /// <param name="playerToken">The user ID</param>
        /// <returns>
        /// The token contains: 
        /// - the player token (player identifier)
        /// - which role(s) the player has (permission security)
        /// </returns>
        private async Task<string> GetApiTokenForPlayer(string playerToken)
        {
            var roles = new List<string>();
            var roleClaims = User.Claims.Where(claim => claim.Type.Equals(ClaimTypes.Role));
            foreach (Claim role in roleClaims)
                roles.Add(role.Value);
            return await _api.GetApiTokenForPlayer(playerToken, roles);
        }

        /// <summary>
        /// Get name for given ID
        /// </summary>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        private string GetPlayerName(string playerToken)
        {
            var user = _userManager.Users.Where(user => user.Id == playerToken).FirstOrDefault();
            return user?.UserName;
        }

        /// <summary>
        /// If the given game exists
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task<bool> GameExists(string token)
        {
            return await _api.GetGame(token) != null;
        }

        /// <summary>
        /// If the currently logged in player is playing in a still ongoing game,
        /// this method will return that game.
        /// If there's multiple ongoing games it will return the first one.
        /// If there's no ongoing games for the player it will return null.
        /// </summary>
        /// <returns>Game|null depending if ongoing game is available for logged in player</returns>
        private async Task<Game> GetUnfinishedGame()
        {
            string playerToken = GetPlayerToken();
            IEnumerable<Game> gamesPlaying = await _api.GetGamesWithPlayer(playerToken);
            return gamesPlaying.FirstOrDefault(g => g.IsFinished == false);
        }

        /// <summary>
        /// Gets token for currently logged in player
        /// </summary>
        /// <returns>player token (string) for currently logged in player</returns>
        private string GetPlayerToken()
        {
            return User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }
    }
}
