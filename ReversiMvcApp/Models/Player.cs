﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReversiMvcApp.Models
{
    public class Player
    {
        [Key]
        public string Token { get; set; }
        public int TimesWon { get; set; } = 0;
        public int TimesLost { get; set; } = 0;
        public int TimesTied { get; set; } = 0;
        [NotMapped]
        public string Name { get; set; }
        
        public Player(string token)
        {
            Token = token;
        }

        public Player(string token, string name, int timesWon, int timesLost, int timesTied) : this(token)
        {
            Name = name;
            TimesWon = timesWon;
            TimesLost = timesLost;
            TimesTied = timesTied;
        }
    }
}
