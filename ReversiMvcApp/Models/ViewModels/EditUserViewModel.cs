﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace ReversiMvcApp.Models.ViewModels
{
    public class EditUserViewModel
    {
        public IdentityUser User { get; set; }
        [Display(Name = "Mediator Role")]
        public bool IsMediator { get; set; }

        public EditUserViewModel(IdentityUser user, bool isMediator)
        {
            User = user;
            IsMediator = isMediator;
        }
    }
}
