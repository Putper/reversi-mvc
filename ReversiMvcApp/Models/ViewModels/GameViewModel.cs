﻿namespace ReversiMvcApp.Models.ViewModels
{
    public class GameViewModel
    {
        public Game Game { get; set; }
        public string PlayerColour { get; set; }
        public string PlayerToken { get; set; }
        public string Player1Name { get; set; }
        public string Player2Name { get; set; }
        public string ApiURI { get; set; }
        public string ApiToken { get; set; }

        public GameViewModel(Game game, string playerColour, string playerToken, string player1Name, string player2Name, string apiURI, string apiToken)
        {
            Game = game;
            PlayerColour = playerColour;
            PlayerToken = playerToken;
            Player1Name = player1Name;
            Player2Name = player2Name;
            ApiURI = apiURI;
            ApiToken = apiToken;
        }
    }
}
