﻿namespace ReversiMvcApp.Models.ViewModels
{
    public class WaitingForPlayerViewModel
    {
        public Game Game { get; set; }
        public string ApiURI { get; set; }
        public string ApiToken { get; set; }

        public WaitingForPlayerViewModel(Game game, string apiURI, string apiToken)
        {
            Game = game;
            ApiURI = apiURI;
            ApiToken = apiToken;
        }
    }
}
