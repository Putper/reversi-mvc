﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ReversiMvcApp.Models.ViewModels
{
    public class UsersViewModel
    {
        public IList<IdentityUser> Users { get; set; }
        public bool IsAdmin { get; set; }

        public UsersViewModel(IList<IdentityUser> users, bool isAdmin)
        {
            Users = users;
            IsAdmin = isAdmin;
        }
    }
}
