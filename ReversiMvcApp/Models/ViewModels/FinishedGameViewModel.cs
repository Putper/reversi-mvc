﻿namespace ReversiMvcApp.Models.ViewModels
{
    public class FinishedGameViewModel
    {
        public GameResult Result { get; set; }

        public FinishedGameViewModel(GameResult result)
        {
            Result = result;
        }
    }
    public enum GameResult
    {
        Won,
        Lost,
        Tied
    }
}
