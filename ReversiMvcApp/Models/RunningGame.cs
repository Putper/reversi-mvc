﻿using System.ComponentModel.DataAnnotations;

namespace ReversiMvcApp.Models
{
    public class RunningGame
    {
        [Key]
        public string Token { get; set; }

        public RunningGame(string token)
        {
            Token = token;
        }
    }
}
