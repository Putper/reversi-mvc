﻿using System.ComponentModel.DataAnnotations;

namespace ReversiMvcApp.Models
{
    public enum Colour { None, White, Black };

    public class Game
    {
        [Key]
        public string Token { get; set; }
        public string Description { get; set; }
        public string Player1Token { get; set; }
        public string Player2Token { get; set; }
        public string Board { get; set; }
        public Colour Turn { get; set; }
        public bool IsFinished { get; set; }
        public Colour Winner { get; set; }
    }
}
